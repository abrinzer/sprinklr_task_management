import React from 'react';
import ReactDOM from 'react-dom';
import TaskApp from './taskApp';

ReactDOM.render(<TaskApp />, document.getElementById('app'));
