import React from 'react';
import TaskView from './TaskView.js';

import ComponentActions from '../actions/ComponentActions.js';

import CreateTask from './CreateTask.js';

var target;
export default React.createClass({

  getInitialState() {
    return {
      hoverTarget: false
    };
  },

  createTasks() {
    return this.props.tasks.map(function(obj) {
      return (<TaskView
        key={obj.id} {...obj}
        projectId={this.props.projectId}
        memberId={this.props.id}
        onDragStart = {this.props.onDragStart}
        onDragStop = {this.props.onDragStop}/>);
    }.bind(this));
  },

  showCreateTask(ev) {
    var pos = {
      top: ev.clientY + "px",
      left: ev.clientX + "px"
    };
    var member = this.props;
    ComponentActions.showOverlay({
      template: CreateTask,
      style: pos,
      onSave: function(data) {
        ComponentActions.createTask({
          projectId: member.projectId,
          memberId: member.id,
          task: data
        });
      }
    });
  },

  onMouseEnterTarget() {
    return this.setState({
      hoverTarget: true
    });
  },

  onMouseLeaveTarget() {
    return this.setState({
      hoverTarget: false
    });
  },

  active() {
    var item, ref1;
    item = this.props.currentDragTask;
    return item && item.memberId !== this.props.id;
  },

  onDrop() {
    if(this.active()) {
      return this.props.onDrop && this.props.onDrop(this.props);
    }
  },

  render() {
    var taskDropTargetClassname = (function(_this) {
      if(_this.state.hoverTarget && _this.props.currentDragTask) {
        return 'drop-target';
      } else return '';
    })(this);
    return (
      <div className='member-view'>
        <div className='header'>
          {this.props.name}
        </div>
        <div className='tasks'>
          {this.createTasks()}
        </div>
        <div className={'new-task task-view ' + taskDropTargetClassname}
          onClick={this.showCreateTask}
          onMouseEnter={this.onMouseEnterTarget}
          onMouseLeave={this.onMouseLeaveTarget}
          onMouseUp={this.onDrop} >
          <div className='new-icon'></div>
          <div className='task-title'>Create Task</div>
        </div>
      </div>
    );
  }
});
