import React from 'react';

import ComponentActions from '../actions/ComponentActions.js';

export default React.createClass({

  getInitialState:function(){
    return {selectValue:1};
  },
  cancelCreate() {
    ComponentActions.hideOverlay();
  },

  createProject() {
    var data = {
      title: this.refs.projectTitle.value,
      desc: this.refs.projectDesc.value,
      memberCount: this.state.selectValue
    };
    ComponentActions.saveOverlay(data);
  },

  handleChange(e){
    this.setState({selectValue: e.target.value})
  },

  render() {
    return (
      <div className='create-dialog create-task'>
        <div className='dialog-header'>
          <div className='header-label'>Create Project</div>
          <div className='close-icon' onClick={this.cancelCreate}></div>
        </div>
        <div className='dialog-content'>
          <div className='form-field'>
            <div className='field-label'>Title</div>
            <div className='input-group'>
              <input type='text' placeholder='Task Title...' ref='projectTitle' />
            </div>
          </div>
          <div className='form-field'>
            <div className='field-label'>Description</div>
            <div className='input-group'>
              <textarea placeholder='Start typing...' ref='projectDesc'></textarea>
            </div>
          </div>
          <div className="form-field">
            <div className='field-label'>Members</div>
            <div className='input-group'>
              <select value={this.state.selectValue} onChange={this.handleChange} >
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
              </select>
            </div>
          </div>
        </div>
        <div className='dialog-actions'>
          <button className='secondary' onClick={this.cancelCreate}>Cancel</button>
          <button className='primary' onClick={this.createProject}>Create</button>
        </div>
      </div>
    );
  }
});
