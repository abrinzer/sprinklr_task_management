## Sprinklr task management app
# steps to run the application
To run the app, please do the following

  1. Go inside the app directory, and run `npm install`
  2. Now run `npm start`. The server will start running on localhost:4000
  4. Open the localhost:4000 on browser.
